package HomeWorks.lesson10.task.model;


import HomeWorks.lesson10.task.service.impl.UserService;

public class Users  {

   public String username;
   public String password;

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Users{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
